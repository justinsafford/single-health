(function () {
    
    angular.module('metrics', ['ngRoute', 'ngSanitize', 'ui.bootstrap']);
    
    function config($routeProvider) {
        $routeProvider
        .when('/', {
            templateUrl: '/home/home.view.html',
            controller: 'homeCtrl',
            controllerAs: 'vm'
        })
        .otherwise({ redirectTo: '/' });
    }
    
    angular
        .module('metrics')
        .config(['$routeProvider', config]);
})();